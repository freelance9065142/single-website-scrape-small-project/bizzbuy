import requests
import json

headers = {
    "authority": "api.bizbuysell.com",
    "accept": "application/json, text/plain, */*",
    "accept-language": "en-US,en;q=0.9,id;q=0.8",
    "authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiI0MTY4YjRmNC1mNDhlLTRjNmUtOTU0MS05NjcwZjE5MjBmZmMiLCJqdGkiOiI2YzUzOTM3Mi0xMjk4LTRmNzYtOTI4Ny0zNjk2OThkM2ZjOTQiLCJlbnMiOiIyMCIsInN1aWQiOiIiLCJiZnNyIjoiMCIsImRpZCI6IjEwIiwidWFpZCI6IjExNzE0OTUiLCJ1aXAiOiIxNzUuMTA2LjguMjQzIiwiZXhwIjoxNjk3MTIyMzU5LCJpc3MiOiJodHRwczovL2FwaS5iaXpidXlzZWxsLmNvbSIsImF1ZCI6ImJpemJ1eXNlbGwifQ.bZFZ65ZrQBYJ4w4S8hwBE8TGMl77BOX_k3sNV7ONDQw",
    "content-type": "application/json",
    "origin": "https://www.bizbuysell.com",
    "referer": "https://www.bizbuysell.com/",
    "sec-ch-ua": '"Google Chrome";v="117", "Not;A=Brand";v="8", "Chromium";v="117"',
    "sec-ch-ua-mobile": "?0",
    "sec-ch-ua-platform": '"Windows"',
    "sec-fetch-dest": "empty",
    "sec-fetch-mode": "cors",
    "sec-fetch-site": "same-site",
    "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36",
    "x-correlation-id": "a746d99b-8c18-4e7f-bf99-6ff57d117429",
}

datas = []

for page in range(1, 201):
    json_data = {
        "bfsSearchCriteria": {
            "siteId": 20,
            "languageId": 10,
            "categories": None,
            "locations": None,
            "excludeLocations": None,
            "askingPriceMax": 0,
            "askingPriceMin": 0,
            "pageNumber": page,
            "keyword": None,
            "cashFlowMin": 0,
            "cashFlowMax": 0,
            "grossIncomeMin": 0,
            "grossIncomeMax": 0,
            "daysListedAgo": 0,
            "establishedAfterYear": 0,
            "listingsWithNoAskingPrice": 0,
            "homeBasedListings": 0,
            "includeRealEstateForLease": 0,
            "listingsWithSellerFinancing": 0,
            "realEstateIncluded": 0,
            "showRelocatableListings": False,
            "relatedFranchises": 0,
            "listingTypeIds": None,
            "designationTypeIds": None,
            "sortList": None,
            "absenteeOwnerListings": 0,
        },
        "industriesHierarchy": 10,
        "industriesFlat": 10,
        "bfsSearchResultsCounts": 0,
        "cmsFilteredData": 0,
        "rightRailBrokers": 0,
        "statesRegions": 10,
        "languageTypeId": 10,
    }

    response = requests.post('https://api.bizbuysell.com/bff/v2/BbsBfsSearchResults', headers=headers, json=json_data)

    if response.status_code == 200:
        datas.append(response.json())
        print(page)

with open(f"D:\\freelance\\restaurant-bussiness-scrapping\\data_bizbuy.json", "a") as file:
    json.dump(datas, file)
