import html
import json
import re
import time
from turtle import title
import requests
import pandas as pd
from bs4 import BeautifulSoup
import pandas as pd

def get_data():

    clean_datas = []

    cookies = {
        '_bbs.uid': '4168b4f4-f48e-4c6e-9541-9670f1920ffc',
        '_initialsource': 'eyJTb3VyY2VWYWx1ZUlEIjowLCJSZWdpc3RyYXRpb25Tb3VyY2VUeXBlSWQiOjAsIlNvdXJjZVVSTCI6Imh0dHBzJTNhJTJmJTJmd3d3LmJpemJ1eXNlbGwuY29tJTJmbGlzdGluZ3MlMmZQcm9maWxlJTJmZGVmYXVsdC5hc3B4JTNmcSUzZDIxNTc1MjglMjZscHNoJTNkaHVnZS1zdGFuZC1hbG9uZS1yZXN0YXVyYW50LWFsbW9zdC03LTAwMC1zcS1mdC1tdWx0aS1taWxsaW9uLWxvY2F0aW9uJTI2bHNlbyUzZDEiLCJEYXRlVGltZUNyZWF0ZWQiOiJcL0RhdGUoMTY5NjkxOTY2Nzk1NSlcLyJ9',
        '_bbs_su_es': 'eyJTb3VyY2VWYWx1ZUlEIjowLCJSZWdpc3RyYXRpb25Tb3VyY2VUeXBlSWQiOjAsIlNvdXJjZVVSTCI6Imh0dHBzJTNhJTJmJTJmd3d3Lmdvb2dsZS5jb20lMmYiLCJEYXRlVGltZUNyZWF0ZWQiOiJcL0RhdGUoMTY5NjkxOTY2Nzk1NSlcLyJ9',
        'IpLocation': '{"IPLocationID":0,"StartingIP":0,"EndingIP":0,"CountryCode":"ID","CountryName":null,"StateProvName":null,"CityName":"Jakarta","PostalCode":"11430","County":null,"StateProvCode":"04","Latitude":-6.13,"Longitude":106.75,"Shard":0,"ActualIP":2942961907,"CountryID":49,"RegionID":null}',
        'IpLocationChecked': 'True',
        '_gcl_au': '1.1.501883094.1696919671',
        'ss_o': 'true',
        'BulletinSrcAdSearch': '1',
        'BizAlertsSignupPopupSeen': 'Thu%2C%2012%20Oct%202023%2010%3A53%3A07%20GMT',
        'ASP.NET_SessionId': 'e1exw4255qkinmxny153aqk1',
        '_track_tkn': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiI0MTY4YjRmNC1mNDhlLTRjNmUtOTU0MS05NjcwZjE5MjBmZmMiLCJqdGkiOiJjMzYwMzA2Yi01Y2YyLTQ5ZWQtYmQ1Yi1hMzM1MThiOWM5YTAiLCJlbnMiOiIyMCIsInN1aWQiOiIiLCJiZnNyIjoiMCIsImRpZCI6IjEwIiwidWFpZCI6IjIxMDU2NjEiLCJ1aXAiOiIxMDMuMTA2Ljc2LjI3IiwiZXhwIjoxNjk3NDM4NDg2LCJpc3MiOiJodHRwczovL2FwaS5iaXpidXlzZWxsLmNvbSIsImF1ZCI6ImJpemJ1eXNlbGwifQ.WCYaGhVmiy49l8fiS3oNIItHVLatTloIeaAESTfubuY',
        '__AntiXsrfToken': 'ebf3912b1c49487cb8615acaf46c3911',
        '_gid': 'GA1.2.302137643.1697424089',
        'bm_mi': '2712D9E9AE30F8A708BA577964546D09~YAAQBa0wF+KcszOLAQAA0ixeNhV0W+ffPEsrk9df53kO2oL/C1jkH2b9XGc6yfi/DnfBCxvEzok2DNUu5N7d7nhV6krLRizj/k8ttKsvn0J1SxZ7BTrLlUnCOjnL8g/R7rBFJ60ZXYDtuhGMGlrZfB1U4BCg8efRb8iCaJKEWL1JufEQCMzEqy0o50WYxmEUiDyyYhEel28k0V42sxOkbRCY/Y/nl+k5LpC5olikMdTgq6A99QxlFsqprjGINnC1kJnjUdVUzJmeCPQqnO9NH3sBq3UARXA7YaNQKynjsLesDX42j5GteamzqC4pWWKsRVfmAOCCaQfLhB9Lpltq3VVkC34VyHx0ecFTdBfgKGbdxNvI/BJ4gfKt8ipSdpIsZE0Wp7xe8BWxto8gp3JEQTBstg56lD8t~1',
        '_gat': '1',
        '_gat_UA-735942-6': '1',
        '_uetsid': '81ffc2c06bcd11eea1cc078ec84caa68',
        '_uetvid': '127c2cc0673711eea7d00fc714b0c36e',
        '_ga_NJNXMWK3N9': 'GS1.1.1697424088.6.1.1697424224.60.0.0',
        '_ga': 'GA1.1.504926729.1696919671',
        'ak_bmsc': '7B964674C0BC0D80FEF6C3E9ADA550E2~000000000000000000000000000000~YAAQBa0wFzudszOLAQAAcTBeNhU3eO5KmVzAH4uDPHUhBi5QOi7qYqwASRjx7ipmMPDP/VcBvNNVhi3Ybq0JxXGhlK5TV36uSDk2G06balsbBCtq5BUqgN+npR1wZNEHnMVMmS8fPQ07ZTZ3J3DukRTVjE+TKdEoPU405nQ+ZojMJYnDmJokeV4j5VIJibbSEpTx17in+7tVBbPITfE0gyaiGYmU1sTIErboIVyG4+gQi+J7O+9CSTlB3jFY+r/fomClE4uQa5rxws/G2xOFkSA9DHaR+wshndOP5145jfenh5QvoUlGW6MWaWFTTtKhRuccKIb4yJuauCIlB3zmlZ4b1E3/Jw7YH5O/B6V7TbKTbHc8mljwvJqD2V+uzE6vEBp5gVhddba2oT20uYiLihWcR2Ezdmu4vhHnYe38mL68kqTRY/T9zOxohaGGAe5vYQKZEWxUMwlz+jWlmCn6Xx0M0L2Kwu1Bo2QpKRxg94cDRaNzcIdijunnup9i7sD7eXCVvTBKcAo8NQq4KGjwSY2Xv4igKr3Y7VlR+hI1Rk2l6u1Ny1lye9QfeHRTH+QiX/2pr2gfMX4TsNB6BqMUMPgNHbR+yp7IGps/nAGVNuPukpa83jFufPPBZ8DA',
        'bm_sv': '28F90F2994C3056FFFEA963009A1D2B9~YAAQr+84F8zqdTGLAQAAjzJeNhW5/M19jwhTkBiBUUXiSgK6UWlmjYckyMQI7xHpxYX8Rpd8sdeLQf9aQMSdqK6p0Qpn0UHONyMoeYwXuQaCTVp1fJ/ylMsqe82fB/ktHQmyHm5AnVk+9+3K20GwJcLTG2I1tjmFuTLxq8sPjDat6j3fpNzjEgfpNzOZdYBarWkmhW4h+HtcHkL+LNgPhX4J5HBlnXPlsR4Dphe6NDBkewiceSE4BUiu1+KqPalAZVpOnQ==~1',
    }

    headers = {
        'authority': 'www.bizbuysell.com',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
        'accept-language': 'en-US,en;q=0.9,id;q=0.8',
        'cache-control': 'max-age=0',
        # 'cookie': '_bbs.uid=4168b4f4-f48e-4c6e-9541-9670f1920ffc; _initialsource=eyJTb3VyY2VWYWx1ZUlEIjowLCJSZWdpc3RyYXRpb25Tb3VyY2VUeXBlSWQiOjAsIlNvdXJjZVVSTCI6Imh0dHBzJTNhJTJmJTJmd3d3LmJpemJ1eXNlbGwuY29tJTJmbGlzdGluZ3MlMmZQcm9maWxlJTJmZGVmYXVsdC5hc3B4JTNmcSUzZDIxNTc1MjglMjZscHNoJTNkaHVnZS1zdGFuZC1hbG9uZS1yZXN0YXVyYW50LWFsbW9zdC03LTAwMC1zcS1mdC1tdWx0aS1taWxsaW9uLWxvY2F0aW9uJTI2bHNlbyUzZDEiLCJEYXRlVGltZUNyZWF0ZWQiOiJcL0RhdGUoMTY5NjkxOTY2Nzk1NSlcLyJ9; _bbs_su_es=eyJTb3VyY2VWYWx1ZUlEIjowLCJSZWdpc3RyYXRpb25Tb3VyY2VUeXBlSWQiOjAsIlNvdXJjZVVSTCI6Imh0dHBzJTNhJTJmJTJmd3d3Lmdvb2dsZS5jb20lMmYiLCJEYXRlVGltZUNyZWF0ZWQiOiJcL0RhdGUoMTY5NjkxOTY2Nzk1NSlcLyJ9; IpLocation={"IPLocationID":0,"StartingIP":0,"EndingIP":0,"CountryCode":"ID","CountryName":null,"StateProvName":null,"CityName":"Jakarta","PostalCode":"11430","County":null,"StateProvCode":"04","Latitude":-6.13,"Longitude":106.75,"Shard":0,"ActualIP":2942961907,"CountryID":49,"RegionID":null}; IpLocationChecked=True; _gcl_au=1.1.501883094.1696919671; ss_o=true; BulletinSrcAdSearch=1; BizAlertsSignupPopupSeen=Thu%2C%2012%20Oct%202023%2010%3A53%3A07%20GMT; ASP.NET_SessionId=e1exw4255qkinmxny153aqk1; _track_tkn=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiI0MTY4YjRmNC1mNDhlLTRjNmUtOTU0MS05NjcwZjE5MjBmZmMiLCJqdGkiOiJjMzYwMzA2Yi01Y2YyLTQ5ZWQtYmQ1Yi1hMzM1MThiOWM5YTAiLCJlbnMiOiIyMCIsInN1aWQiOiIiLCJiZnNyIjoiMCIsImRpZCI6IjEwIiwidWFpZCI6IjIxMDU2NjEiLCJ1aXAiOiIxMDMuMTA2Ljc2LjI3IiwiZXhwIjoxNjk3NDM4NDg2LCJpc3MiOiJodHRwczovL2FwaS5iaXpidXlzZWxsLmNvbSIsImF1ZCI6ImJpemJ1eXNlbGwifQ.WCYaGhVmiy49l8fiS3oNIItHVLatTloIeaAESTfubuY; __AntiXsrfToken=ebf3912b1c49487cb8615acaf46c3911; _gid=GA1.2.302137643.1697424089; bm_mi=2712D9E9AE30F8A708BA577964546D09~YAAQBa0wF+KcszOLAQAA0ixeNhV0W+ffPEsrk9df53kO2oL/C1jkH2b9XGc6yfi/DnfBCxvEzok2DNUu5N7d7nhV6krLRizj/k8ttKsvn0J1SxZ7BTrLlUnCOjnL8g/R7rBFJ60ZXYDtuhGMGlrZfB1U4BCg8efRb8iCaJKEWL1JufEQCMzEqy0o50WYxmEUiDyyYhEel28k0V42sxOkbRCY/Y/nl+k5LpC5olikMdTgq6A99QxlFsqprjGINnC1kJnjUdVUzJmeCPQqnO9NH3sBq3UARXA7YaNQKynjsLesDX42j5GteamzqC4pWWKsRVfmAOCCaQfLhB9Lpltq3VVkC34VyHx0ecFTdBfgKGbdxNvI/BJ4gfKt8ipSdpIsZE0Wp7xe8BWxto8gp3JEQTBstg56lD8t~1; _gat=1; _gat_UA-735942-6=1; _uetsid=81ffc2c06bcd11eea1cc078ec84caa68; _uetvid=127c2cc0673711eea7d00fc714b0c36e; _ga_NJNXMWK3N9=GS1.1.1697424088.6.1.1697424224.60.0.0; _ga=GA1.1.504926729.1696919671; ak_bmsc=7B964674C0BC0D80FEF6C3E9ADA550E2~000000000000000000000000000000~YAAQBa0wFzudszOLAQAAcTBeNhU3eO5KmVzAH4uDPHUhBi5QOi7qYqwASRjx7ipmMPDP/VcBvNNVhi3Ybq0JxXGhlK5TV36uSDk2G06balsbBCtq5BUqgN+npR1wZNEHnMVMmS8fPQ07ZTZ3J3DukRTVjE+TKdEoPU405nQ+ZojMJYnDmJokeV4j5VIJibbSEpTx17in+7tVBbPITfE0gyaiGYmU1sTIErboIVyG4+gQi+J7O+9CSTlB3jFY+r/fomClE4uQa5rxws/G2xOFkSA9DHaR+wshndOP5145jfenh5QvoUlGW6MWaWFTTtKhRuccKIb4yJuauCIlB3zmlZ4b1E3/Jw7YH5O/B6V7TbKTbHc8mljwvJqD2V+uzE6vEBp5gVhddba2oT20uYiLihWcR2Ezdmu4vhHnYe38mL68kqTRY/T9zOxohaGGAe5vYQKZEWxUMwlz+jWlmCn6Xx0M0L2Kwu1Bo2QpKRxg94cDRaNzcIdijunnup9i7sD7eXCVvTBKcAo8NQq4KGjwSY2Xv4igKr3Y7VlR+hI1Rk2l6u1Ny1lye9QfeHRTH+QiX/2pr2gfMX4TsNB6BqMUMPgNHbR+yp7IGps/nAGVNuPukpa83jFufPPBZ8DA; bm_sv=28F90F2994C3056FFFEA963009A1D2B9~YAAQr+84F8zqdTGLAQAAjzJeNhW5/M19jwhTkBiBUUXiSgK6UWlmjYckyMQI7xHpxYX8Rpd8sdeLQf9aQMSdqK6p0Qpn0UHONyMoeYwXuQaCTVp1fJ/ylMsqe82fB/ktHQmyHm5AnVk+9+3K20GwJcLTG2I1tjmFuTLxq8sPjDat6j3fpNzjEgfpNzOZdYBarWkmhW4h+HtcHkL+LNgPhX4J5HBlnXPlsR4Dphe6NDBkewiceSE4BUiu1+KqPalAZVpOnQ==~1',
        'sec-ch-ua': '"Chromium";v="118", "Google Chrome";v="118", "Not=A?Brand";v="99"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
        'sec-fetch-dest': 'document',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-user': '?1',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36',
    }

    cont = False

    with open(
        f"D:\\freelance\\restaurant-bussiness-scrapping\\bizzbuy\\link_bizbuy.json", "r"
    ) as file:
        json_file = json.load(file)
        with open(f"D:\\freelance\\restaurant-bussiness-scrapping\\bizzbuy\\data.json", "a") as data_file:
            for data in json_file:
                if data.get("value"):
                    value = data["value"]
                    search = value["bfsSearchResult"]
                    raw_data = search.get("value")
                    for data in raw_data:
                        url_stub = data.get("urlStub")
                        if url_stub == '/Business-Opportunity/established-hotworx-franchise-for-sale/2100977/':
                            cont = True
                        if cont:
                            print(url_stub)
                            clean_data = {
                                "Title": data.get("header"),
                                "Asking Price": data.get("price"),
                                "Cash Flow": data.get("cashFlow"),
                                "Description": data.get("description"),
                                'URL': f"https://www.bizbuysell.com{url_stub}"
                            }
                            try:
                                response = requests.get(
                                    f"https://www.bizbuysell.com{url_stub}",
                                    headers=headers,
                                    cookies=cookies
                                )
                                if response.status_code == 200:
                                    soup = BeautifulSoup(response.content, "html.parser")
                                    row_fluid_outer = soup.find(
                                        "div", class_="row-fluid b-margin financials clearfix"
                                    )
                                    row_fluids = row_fluid_outer.findAll("div", class_="span6 specs")
                                    key = []
                                    value = []
                                    for row_fluid in row_fluids:
                                        span_elements = row_fluid.findAll("span", class_="title")
                                        for span in span_elements:
                                            if ":" in span.text:
                                                clean_span = html.unescape(span.text.replace(":", ""))
                                                key.append(clean_span)
                                        b_elements = row_fluid.findAll("b")
                                        for b in b_elements:
                                            clean_b = html.unescape(b.text.strip())
                                            match = re.search(r'\$([\d,]+)\*?', clean_b)
                                            if not match:
                                                clean_b = None
                                            else:
                                                clean_b = int(match.group(1).replace(',', ''))
                                            value.append(clean_b)
                                    for index, data_key in enumerate(key):
                                        clean_data[data_key] = value[index]
                                    ad_lines = soup.find_all("b", class_="profileAdLine")
                                    for ad_line in ad_lines:
                                        clean_data["Ad Line"] = html.unescape(ad_line.text)
                                    dls = soup.findAll("dl", class_="listingProfile_details")
                                    for dl in dls:
                                        dt = html.unescape(dl.dt.text.replace(":", ""))
                                        dd = html.unescape(dl.dd.text)
                                    data_file.writelines(f'{json.dumps(clean_data)}\n')
                            except Exception:
                                continue

def to_csv():
    data = []
    with open(f"D:\\freelance\\restaurant-bussiness-scrapping\\bizzbuy\\data.json", "r") as file:
        lines = file.readlines()
        for line in lines:
            raw = json.loads(line)
            data.append(raw)
    df = pd.DataFrame(data)
    df.to_csv('D:\\freelance\\restaurant-bussiness-scrapping\\bizzbuy\\data_bizzbuy.csv', index=False)
    print('done')
to_csv()

